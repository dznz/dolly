import 'dart:async';

import 'package:dolly/dolly.dart';
import 'package:dolly/src/context.dart';
import 'package:test/test.dart';

import 'helpers.dart';

void main() {
  group('Policy.execute()', () {
    test('executes the specified action', () {
      var executed = false;
      var policy = Policy.handle<UnfiredException>().retryForever();
      policy.execute(() {
        executed = true;
      });
      expect(executed, isTrue);
    });

    test('returns a result', () {
      var policy = Policy.handle<UnfiredException>().retryForever();
      var result = policy.execute(() => 1);
      expect(result, 1);
    });

    test('makes a context available', () {
      var policy = Policy.handle<UnfiredException>().retryForever();
      policy.executeWithContext(
          (context) => expect(context, TypeMatcher<Context>()), {'a': 'b'});
    });
  });

  group('Policy.executeAsync()', () {
    test('executes the async function', () async {
      var executed = false;
      var policy = Policy.handle<UnfiredException>().retryForever();
      await policy.executeAsync(() async {
        executed = true;
        return await Future.value(() {});
      });
      expect(executed, isTrue);
    });
    test('returns a result', () async {
      var policy = Policy.handle<UnfiredException>().retryForever();
      var result = await policy.executeAsync(() => Future.value(1));
      expect(result, 1);
    });

    test('makes a context available', () {
      var policy = Policy.handle<UnfiredException>().retryForever();
      policy.executeWithContextAsync((context) async {
        expect(context, TypeMatcher<Context>());
        expect(context['a'], 'b');
        return await Future.value(true);
      }, {'a': 'b'});
    });
  });

  group('NoOp', () {
    test('Executes provided delegate', () {
      var policy = Policy.noOp();
      var executed = false;

      policy.execute(() {
        executed = true;
      });

      expect(executed, isTrue);
    });

    test('handles returns', () {
      var policy = Policy.noOp();
      var result = policy.execute((ctx) => 1);
      expect(result, 1);
    });
  });
  group('Retry', () {
    group('execute()', () {
      test('Executes provided delegate', () {
        var executed = false;
        var policy = Policy.handle<UnfiredException>().retry(1);

        policy.execute(() {
          executed = true;
        });

        expect(executed, isTrue);
      });

      test('throws if only onRetryAsync provided', () {
        var policy = Policy.handle<UnfiredException>().retry()
          ..onRetryAsync = (eor, ctx, [i]) async => 2;

        expect(() => policy.execute(() => 0), throwsStateError);
      });
    });

    test('Retries on error', () {
      var policy = Policy.handle<TestException>().retry();

      policy.execute(failNTimes(() => TestException(), 1));
    });

    test('Retries on N errors', () {
      var policy = Policy.handle<TestException>().retry(3);

      policy.execute(failNTimes(() => TestException(), 3));
    });

    test('Executes onRetry callback', () {
      var retryCallbackExecuted = false;
      var policy = Policy.handle<TestException>().retry(1)
        ..onRetry = (a, b, [c]) {
          retryCallbackExecuted = true;
        };

      policy.execute(failNTimes(() => TestException(), 1));

      expect(retryCallbackExecuted, isTrue);
    });

    group('executeAsync()', () {
      test('calls onRetryAsync callback', () async {
        var retryCallbackExecuted = false;
        var policy = Policy.handle<TestException>().retry(1)
          ..onRetryAsync = (a, b, [c]) async {
            retryCallbackExecuted = true;
            await Future.value(null);
          };

        await policy
            .executeAsync(await failNTimesAsync(() => TestException(), 1));

        expect(retryCallbackExecuted, isTrue);
      });

      test('when no onRetryAsync provided, call wrapped onRetry', () async {
        var retryCallbackExecuted = false;
        var policy = Policy.handle<TestException>().retry()
          ..onRetry = (eor, ctx, [i]) {
            retryCallbackExecuted = true;
          };

        await policy
            .executeAsync(await failNTimesAsync(() => TestException(), 1));

        expect(retryCallbackExecuted, isTrue);
      });
    });

    test('captures on any exception', () {
      var wasSecondExceptionCaught = false;
      var policy =
          Policy.handle<UnfiredException>().or<TestException>().retry(1)
            ..onRetry = (e, c, [i]) {
              wasSecondExceptionCaught = true;
            };

      policy.execute(failNTimes(() => TestException(), 1));

      expect(wasSecondExceptionCaught, isTrue);
    });

    test('captures bad results', () {
      var retryCalled = false;
      var policy = Policy.handleResult<int>(500).retry()
        ..onRetry = ((ex, ctx, [i]) => retryCalled = true);
      var attemptCount = 0;

      policy.execute(() {
        attemptCount++;
        if (attemptCount <= 1) {
          return 500;
        } else {
          return 200;
        }
      });

      expect(retryCalled, true);
    });

    test('captures multiple result types', () {
      var builder = Policy.handleResult(500).orResult(404);
      expect(builder.resultPredicates.length, 2);

      var callCount = 0;
      var policy = builder.retry()..onRetry = (eor, ctx, [i]) => callCount++;

      policy.execute(() => 500);
      expect(callCount, 1);

      policy.execute(() => 404);
      expect(callCount, 2);
    });
  });

  group('Context', () {
    test('can be coerced', () {
      var context = Context.fromData({'key1': 'value1'});
      expect(context['key1'], 'value1');
    });
    test('assigns operation key on construction', () {
      var context = Context('opKey');

      expect(context.operationKey, 'opKey');
    });

    test('assigns op key and context data on construction', () {
      var context = Context('opKey', {'key1': 'value1', 'key2': 'value2'});
      expect(context.operationKey, 'opKey');
      expect(context['key1'], 'value1');
      expect(context['key2'], 'value2');
    });

    test('operationkey may be null', () {
      var context = Context();
      expect(context.operationKey, isNull);
    });

    test('assigns correlationId when accessed', () {
      var context = Context();
      expect(context.correlationId, isNotNull);
    });

    test('returns a consistent correlationId', () {
      var context = Context();
      var retrieved1 = context.correlationId;
      expect(context.correlationId, equals(retrieved1));
    });
  });

  group('RetryForever', () {
    test('retries FOREVER', () {
      var attemptCount = 0;
      var policy = Policy.handle<TestException>().retryForever()
        ..onRetry = (ex, context, [errorCount]) => attemptCount++;

      policy.execute(failNTimes(() => TestException(), 3));

      expect(attemptCount, 3);
    });

    test('passes unhandled exceptions', () {
      var policy = Policy.handle<TestException>().retryForever();

      expect(() {
        policy.execute(() {
          throw Exception();
        });
      }, throwsException);
    });

    test('Executes onRetry callback', () {
      var retryCallbackExecuted = false;
      var policy = Policy.handle<TestException>().retryForever()
        ..onRetry = ((a, b, [c]) {
          retryCallbackExecuted = true;
        });

      policy.execute(failNTimes(() => TestException(), 1));

      expect(retryCallbackExecuted, isTrue);
    });
  });
}
