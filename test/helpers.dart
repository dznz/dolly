import 'dart:async';

class TestException implements Exception {
  bool shouldBeFiltered = false;

  TestException({this.shouldBeFiltered});
}

class UnfiredException implements Exception {}

Function failNTimes(Function exceptionBlock, int n) {
  var attemptCount = 0;
  return () {
    attemptCount++;
    if (attemptCount <= n) {
      throw exceptionBlock();
    }
  };
}

Function failNTimesAsync(Function exceptionBlock, int n) {
  var attemptCount = 0;
  return () async {
    attemptCount++;
    if (attemptCount <= n) {
      throw exceptionBlock();
    }
    await Future.value(null);
  };
}
