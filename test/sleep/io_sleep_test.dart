@TestOn('vm')

import 'package:dolly/src/sleep/io_sleep.dart';
import 'package:dolly/src/sleep/sleep.dart';
import 'package:test/test.dart';

void main() {
  test('works for io', () {
    var sut = Sleep();
    expect(sut, TypeMatcher<IOSleep>());
    sut.sleep(Duration(milliseconds: 1));
  });
}
