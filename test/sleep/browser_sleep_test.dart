@TestOn('browser')

import 'package:dolly/src/sleep/browser_sleep.dart';
import 'package:dolly/src/sleep/sleep.dart';
import 'package:test/test.dart';

void main() {
  test('works for browser', () {
    var sut = Sleep();
    expect(sut, TypeMatcher<BrowserSleep>());
    sut.sleep(Duration(milliseconds: 1));
  });
}
