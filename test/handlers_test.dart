import 'package:dolly/dolly.dart';
import 'package:test/test.dart';

import 'helpers.dart';

void main() {
  group('handle()', () {
    test('handles a thing', () {
      var builder = Policy.handle<UnfiredException>();
      expect(builder.runtimeType, PolicyBuilder);
    });

    test('optionally allows filtering', () {
      var policy = Policy.handle<TestException>(
          (e) => (e as TestException).shouldBeFiltered).retry();

      policy
          .execute(failNTimes(() => TestException(shouldBeFiltered: true), 1));

      expect(
          () => policy
              .execute(() => throw TestException(shouldBeFiltered: false)),
          throwsA(TypeMatcher<TestException>()));
    });
  });

  group('handleResult()', () {
    test('takes a value', () {
      var policy = Policy.handleResult<int>(500).retry();

      policy.execute(() => 500);
    });

    test('takes a predicate', () {
      var policy = Policy.handleResult<int>((r) => r == 500).retry();

      policy.execute(() => 500);
    });

    test('passes with different results', () {
      var callbackExecuted = false;
      var policy = Policy.handleResult<int>(500).retry()
        ..onRetry = (eor, ctx, [i]) => callbackExecuted = true;

      policy.execute(() => 200);

      expect(callbackExecuted, isFalse);
    });
  });

  group('or()', () {
    test('adds another exception', () {
      var builder = Policy.handle<UnfiredException>()..or<TestException>();

      expect(builder, TypeMatcher<PolicyBuilder>());
      expect(builder.exceptionPredicates.length, 2);
    });
  });

  group('orResult()', () {
    test('adds another result to check', () {
      Policy.handle<UnfiredException>()
        ..orResult<int>(500)
        ..retryForever();
    });
  });
}
