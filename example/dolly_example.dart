import 'package:dolly/dolly.dart';

main() {
  var policy = Policy.handle<Exception>().retryForever();
  policy.execute(() => 'awesome');

  // Handle return value with condition
  Policy.handleResult<int>((r) => r.StatusCode == 404);

  // Handle primitive return values (implied use of .Equals())
  Policy.handleResult<int>(404);

  // Handle both exceptions and return values in one policy
  var httpStatusCodesWorthRetrying = [404, 500];
  Policy.handleResult<int>((r) => httpStatusCodesWorthRetrying.contains(r))
      .or<Exception>()
      .retry();
}
