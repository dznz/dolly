import 'dart:async';

import 'package:dolly/src/context.dart';
import 'package:dolly/src/noop.dart';
import 'package:dolly/src/policy_builder.dart';

abstract class Engine {
  dynamic call(dynamic action, Context context,
      [Map<String, dynamic> callbacks]);
}

/// The simplest kind of action that can be passed to the execute* methods.
typedef Action = dynamic Function();

/// Action that receives a [Context] from executing Policy.
typedef ContextAction = dynamic Function(dynamic ctx);

abstract class Policy {
  Engine engine;

  Engine engineAsync;

  Policy(this.engine, this.engineAsync);

  /// Execute an action, protected by the Policy.
  TResult execute<TResult>(dynamic action) => executeWithContext(action, {});

  TResult executeWithContext<TResult>(dynamic action,
      [Map<String, Object> contextData]) {
    var context = Context.fromData(contextData);
    var callbacks = setCallbacksToSync();
    return engine(action, context, callbacks);
  }

  Future<T> executeAsync<T>(dynamic action) async {
    var callbacks = setCallbacksToAsync();
    return await engineAsync(action, Context(), callbacks);
  }

  Future<TResult> executeWithContextAsync<TResult>(dynamic action,
      [Map<String, Object> contextData]) async {
    var context = Context.fromData(contextData);
    var callbacks = setCallbacksToAsync();
    return await engineAsync(action, context, callbacks);
  }

  /// Return a new [NoOpPolicy].
  static NoOpPolicy noOp() => NoOpPolicy(NoOpEngine(), NoOpEngineAsync());

  static PolicyBuilder handle<TException extends Exception>(
      [ExceptionPredicate exceptionPredicate]) {
    ExceptionPredicate predicate;
    if (exceptionPredicate != null) {
      predicate = (exception) =>
          exception is TException && exceptionPredicate(exception);
    } else {
      predicate = (exception) => exception is TException;
    }
    return PolicyBuilder(predicate);
  }

  /// Specifies the type of result the [Policy] will handle and either a result
  /// value or predicate checking the result.
  ///
  /// This policy filter matches the result value returned using `==`, ideally
  /// suited for value types such as int and enum.  To match characteristics of
  /// class return types, consider passing in a result predicate.
  static PolicyBuilder handleResult<TResult>([dynamic resultOrPredicate]) =>
      PolicyBuilder.fromResult(resultOrPredicate);

  Map<String, dynamic> setCallbacksToSync();

  Map<String, dynamic> setCallbacksToAsync();
}
