import 'dart:collection';
import 'package:uuid/uuid.dart';

/// Context that carries with a single execution through a Policy. Commonly-used properties are directly on the class.
///
/// Backed by a dictionary of string key / object value pairs, to which user-defined values may be added.
/// Do not re-use an instance of [Context] across more than one call through [Policy.Execute()] or [Policy.ExecuteAsync()]
class Context extends Object with MapMixin {
  /// A key unique to the call site of the current execution.
  ///
  /// [Policy] instances are commonly reused across multiple call sites.  Set an OperationKey so that logging and
  /// metrics can distinguish usages of policy instances at different call sites.
  String operationKey;

  Map<String, Object> _wrappedMap;

  String _correlationId;

  Context([this.operationKey, Map<String, Object> contextData = const {}]) {
    _wrappedMap = contextData;
  }

  Context.fromData(Map<String, Object> contextData) : this(null, contextData);

  String get correlationId {
    if (_correlationId == null) {
      _correlationId = Uuid().v4();
    }
    return _correlationId;
  }

  @override
  dynamic operator [](Object key) => _wrappedMap[key];

  @override
  void operator []=(dynamic key, dynamic value) {
    _wrappedMap[key] = value;
  }

  @override
  void clear() {
    _wrappedMap.clear();
  }

  @override
  Iterable get keys => _wrappedMap.keys;

  @override
  dynamic remove(Object key) {
    _wrappedMap.remove(key);
  }
}
