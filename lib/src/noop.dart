import 'package:dolly/src/context.dart';
import 'package:dolly/src/policy.dart';

class NoOpPolicy extends Policy {
  NoOpPolicy(NoOpEngine engine, NoOpEngineAsync engineAsync)
      : super(engine, engineAsync);

  Map<String, dynamic> setCallbacksToSync() => {};

  Map<String, dynamic> setCallbacksToAsync() => {};
}

class NoOpEngineAsync extends Engine {
  dynamic call(dynamic action, Context context,
      [Map<String, dynamic> callbacks]) async {
    if (action is Action) {
      return await action();
    } else if (action is ContextAction) {
      return await action(context);
    } else {
      throw NoSuchMethodError;
    }
  }
}

class NoOpEngine extends Engine {
  dynamic call(dynamic action, Context context, [callbacks]) {
    if (action is Action) {
      return action();
    } else if (action is ContextAction) {
      return action(context);
    } else {
      throw NoSuchMethodError;
    }
  }
}
