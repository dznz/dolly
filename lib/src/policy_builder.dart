import 'package:dolly/src/retry.dart';
import 'package:meta/meta.dart';

typedef ExceptionPredicate = bool Function(Exception ex);
typedef ResultPredicate = bool Function(dynamic result);

class PolicyBuilder {
  @visibleForTesting
  List<ExceptionPredicate> exceptionPredicates = [];

  @visibleForTesting
  List<ResultPredicate> resultPredicates = [];

  PolicyBuilder(ExceptionPredicate exceptionPredicate) {
    if (exceptionPredicate != null) {
      exceptionPredicates.add(exceptionPredicate);
    }
  }

  PolicyBuilder.fromResult([dynamic resultOrPredicate]) {
    addResultOrPredicate(resultOrPredicate);
  }

  void addResultOrPredicate<TResult>([dynamic resultOrPredicate]) {
    var predicate = resultOrPredicate;
    if (resultOrPredicate is TResult) {
      predicate = (r) =>
          (r != null && r == resultOrPredicate) ||
          (r == null && resultOrPredicate == null);
    }
    resultPredicates.add(predicate);
  }

  /// Factory that returns a new [RetryPolicy] that will retry [retryCount]
  /// times before passing through.
  RetryPolicy retry([int retryCount = 1]) => RetryPolicy(
      RetryEngine(
          exceptionPredicates,
          resultPredicates,
          (callbacks) =>
              RetryPolicyStateWithCount(retryCount, callbacks, null)),
      RetryEngineAsync(
          exceptionPredicates,
          resultPredicates,
          (callbacks) =>
              RetryPolicyStateWithCount(retryCount, callbacks, null)));

  /// Factory that returns a new [RetryPolicy] that will attempt to retry a
  /// failing action forever.
  RetryPolicy retryForever() => RetryPolicy(
      RetryEngine(exceptionPredicates, resultPredicates,
          (callbacks) => RetryPolicyState(callbacks, null)),
      RetryEngineAsync(exceptionPredicates, resultPredicates,
          (callbacks) => RetryPolicyState(callbacks, null)));

  /// Add another exception to be handled
  // ignore: avoid_returning_this
  PolicyBuilder or<TException extends Exception>() {
    exceptionPredicates.add((exception) => exception is TException);
    return this;
  }

  PolicyBuilder orResult<TResult>([dynamic resultOrPredicate]) {
    addResultOrPredicate<TResult>(resultOrPredicate);
    return this;
  }
}
