/// The captured outcome of executing an individual action.
///
/// Union type that holds either thrown [Exception] or a [TResult] result value.
class ExceptionOrResult<TResult> {
  /// An exception thrown while executing the action. Will be null if policy
  /// executed without exception.
  Exception exception;

  /// The result of executing the action. Will be null if an exception was
  /// thrown.
  TResult result;

  ExceptionOrResult.fromException(this.exception);

  ExceptionOrResult.fromResult(this.result);
}
