import 'package:dolly/src/sleep/sleep.dart';

/// Used from conditional imports, matches the definition in `io_sleep.dart`.
createSleep() => BrowserSleep();

/// A fallback busywait sleep that can run in the browser.
///
/// Because system sleep isn't available in a browser context, we have to use a
/// Busy Wait style sleep. This will lock up your (single-threaded) Dart app in
/// the browser, but that is presumably what you asked for with a synchronous
/// wait.
///
/// Further reading: https://en.wikipedia.org/wiki/Busy_waiting
class BrowserSleep implements Sleep {
  void sleep(Duration duration) {
    // busy spin
    var sw = Stopwatch()..start();
    while (sw.elapsed < duration) {
      // Busy Wait
    }
    sw.stop();
  }
}
