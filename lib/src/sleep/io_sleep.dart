import 'dart:io' as io;

import 'package:dolly/src/sleep/sleep.dart';

/// Factory that returns an [IOSleep].
createSleep() => IOSleep();

/// A `dart.library.io`-based sleep that uses system sleep directly.
class IOSleep implements Sleep {
  void sleep(Duration duration) {
    io.sleep(duration);
  }
}
