// ignore: uri_does_not_exist
import 'browser_sleep.dart'
    // ignore: uri_does_not_exist
    if (dart.library.io) 'io_sleep.dart';

/// Abstraction for the sleep function.
///
/// Uses the system sleep if it's available and falls back to a busy wait
/// strategy when `dart:io` is not present i.e. in the browser.
abstract class Sleep {
  factory Sleep() => createSleep();

  void sleep(Duration timespan);
}
