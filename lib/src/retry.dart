import 'dart:async';

import 'package:dolly/src/context.dart';
import 'package:dolly/src/exception_or_result.dart';
import 'package:dolly/src/policy.dart';
import 'package:dolly/src/policy_builder.dart';

class RetryPolicy extends Policy {
  dynamic Function(ExceptionOrResult exception, Context context, [int count])
      onRetry;
  void Function(ExceptionOrResult exception, Context context, [int count])
      onRetryAsync;
  _doNothingRetry(final ExceptionOrResult _, final Context __,
      [final int ___]) {}

  RetryPolicy(RetryEngine retryEngine, RetryEngineAsync retryEngineAsync)
      : super(retryEngine, retryEngineAsync);

  Map<String, dynamic> setCallbacksToSync() {
    if (onRetry == null && onRetryAsync != null) {
      throw StateError(
          'You only set onRetryAsync - did you mean to call the synchronous execute()?');
    }
    return {'onRetry': onRetry ?? _doNothingRetry};
  }

  Map<String, dynamic> setCallbacksToAsync() {
    var alternative = (onRetry != null)
        ? (ex, ctx, [i]) async => onRetry(ex, ctx, i)
        : _doNothingRetry;
    return {'onRetry': onRetryAsync ?? alternative};
  }
}

class RetryEngineAsync extends Engine {
  List<ExceptionPredicate> shouldRetryPredicates;

  List<ResultPredicate> shouldRetryResultPredicates;

  dynamic stateFactory;

  RetryEngineAsync(this.shouldRetryPredicates, this.shouldRetryResultPredicates,
      this.stateFactory);

  call(action, Context context, [Map<String, dynamic> callbacks]) async {
    var policyState = stateFactory(callbacks);
    while (true) {
      try {
        dynamic result;
        if (action is Action) {
          result = await action();
        } else if (action is ContextAction) {
          result = await action(context);
        } else {
          throw NoSuchMethodError;
        }

        if (!shouldRetryResultPredicates
            .any((predicate) => predicate(result))) {
          return result;
        }

        if (!policyState.canRetry(ExceptionOrResult.fromResult(result))) {
          return result;
        }
      } on Exception catch (ex) {
        if (!shouldRetryPredicates.any((predicate) => predicate(ex))) {
          rethrow;
        }

        if (!await policyState
            .canRetryAsync(ExceptionOrResult.fromException(ex))) {
          rethrow;
        }
      }
    }
  }
}

class RetryEngine extends Engine {
  List<ExceptionPredicate> shouldRetryPredicates;
  List<ResultPredicate> shouldRetryResultPredicates;

  dynamic stateFactory;

  RetryEngine(this.shouldRetryPredicates, this.shouldRetryResultPredicates,
      this.stateFactory);

  call(dynamic action, [context, callbacks]) {
    var policyState = stateFactory(callbacks);
    while (true) {
      try {
        dynamic result;
        if (action is Action) {
          result = action();
        } else if (action is ContextAction) {
          result = action(context);
        } else {
          throw NoSuchMethodError;
        }
        if (!shouldRetryResultPredicates
            .any((predicate) => predicate(result))) {
          return result;
        }

        if (!policyState.canRetry(ExceptionOrResult.fromResult(result))) {
          return result;
        }
      } on Exception catch (ex) {
        if (!shouldRetryPredicates.any((predicate) => predicate(ex))) {
          rethrow;
        }

        if (!policyState.canRetry(ExceptionOrResult.fromException(ex))) {
          rethrow;
        }
      }
    }
  }
}

typedef RetryCallback = void Function(ExceptionOrResult ex, Context context,
    [int retryCount]);

class RetryPolicyState {
  Context context;

  RetryCallback onRetry;

  RetryPolicyState(callbacks, [this.context]) {
    onRetry = callbacks['onRetry'];
  }

  bool canRetry(ExceptionOrResult delegateResult) {
    onRetry(delegateResult, context);
    return true;
  }

  Future<bool> canRetryAsync(ExceptionOrResult delegateResult) async {
    await onRetry(delegateResult, context);
    return true;
  }
}

class RetryPolicyStateWithCount implements RetryPolicyState {
  int retryCount;

  RetryCallback onRetry;

  Context context;

  int errorCount = 0;

  RetryPolicyStateWithCount(
      this.retryCount, Map<String, dynamic> callbacks, this.context) {
    onRetry = callbacks['onRetry'];
  }

  bool canRetry(ExceptionOrResult delegateResult) {
    errorCount++;
    var shouldRetry = errorCount <= retryCount;
    if (shouldRetry) {
      onRetry(delegateResult, context, errorCount);
    }
    return shouldRetry;
  }

  Future<bool> canRetryAsync(ExceptionOrResult delegateResult) async {
    errorCount++;
    var shouldRetry = errorCount <= retryCount;
    if (shouldRetry) {
      await onRetry(delegateResult, context, errorCount);
    }
    return shouldRetry;
  }
}
