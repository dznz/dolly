# Dolly

> Fault-tolerance and resilience package for Dart.

## Install

1. Add the following to your project's pubspec.yaml]() and run `pub get`.

```
dependencies:
  dolly: any
```

2. Add the correct import for your project.

```
import 'package:dolly/dolly.dart';
```

## Usage

A simple usage example:

```dart
import 'package:dolly/dolly.dart';

main() {
  var awesome = new Awesome();
}
```

## Contribute

PRs accepted. Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme

## License

ISC © Spark New Zealand Limited

SEE LICENSE IN [LICENSE]()
